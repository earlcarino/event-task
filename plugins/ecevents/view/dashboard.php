<div class="container">
	<h1>FAMOSO DASHBOARD</h1>
	<div class="col-md-12">
	<div class="row">
		<div class="col-md-3"><label>Number of Users: </label></div>
		<div class="col-md-4">
			<input type="number" id="numberUser" name="numberUser" min="0" max="500" value="" />
		</div>
	</div>	
	<div class="row">
		<div class="col-md-3"><label>Gender: </label></div>
		<div class="col-md-4">
			<div class="radio">
			  	<label>
			    	<input class="form-control" type="radio" name="gender" value="male" id="male" checked>
			    	Male
			  	</label>
			</div>
			<div class="radio">
			  	<label>
			    	<input class="form-control" type="radio" name="gender" value="female" id="female" checked>
			    	Female
			  	</label>
			</div>
			<div class="radio">
			  	<label>
			    	<input class="form-control" type="radio" name="gender" value="Random" id="Random" checked>
			    	Random
			  	</label>
			</div>		
		</div>
	</div>	
	<div class="row">	
		<div class="col-md-3"><label>Region: </label></div>
		<div class="col-md-4">
			<select class="form-control" id="regionoptionst" role="optionstbox">
				<option id="region-48" class=" fav active" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -940px;"></span><span class="region-label">United States</span></option>			
				<option id="region-0" role="option" tabindex="-1"><span class="region-label">Random</span></option>
				<option id="region-1" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -0px;"></span><span class="region-label">Albania</span></option>
				<option id="region-2" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -20px;"></span><span class="region-label">Argentina</span></option>
				<option id="region-3" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -40px;"></span><span class="region-label">Armenia</span></option>
				<option id="region-4" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -60px;"></span><span class="region-label">Austraoptiona</span></option>
				<option id="region-5" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -80px;"></span><span class="region-label">Austria</span></option>
				<option id="region-6" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -100px;"></span><span class="region-label">Azerbaijan</span></option>
				<option id="region-7" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -120px;"></span><span class="region-label">Bangladesh</span></option>
				<option id="region-8" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -140px;"></span><span class="region-label">Belgium</span></option>
				<option id="region-9" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -160px;"></span><span class="region-label">Bosnia and Herzegovina</span></option>
				<option id="region-10" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -180px;"></span><span class="region-label">Brazil</span></option>
				<option id="region-11" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -200px;"></span><span class="region-label">Canada</span></option>
				<option id="region-12" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -220px;"></span><span class="region-label">China</span></option>
				<option id="region-13" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -240px;"></span><span class="region-label">Colombia</span></option>
				<option id="region-14" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -260px;"></span><span class="region-label">Denmark</span></option>
				<option id="region-15" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -280px;"></span><span class="region-label">Egypt</span></option>
				<option id="region-16" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -300px;"></span><span class="region-label">England</span></option>
				<option id="region-17" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -320px;"></span><span class="region-label">Estonia</span></option>
				<option id="region-18" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -340px;"></span><span class="region-label">Finland</span></option>
				<option id="region-19" class=" fav" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -360px;"></span><span class="region-label">France</span></option>
				<option id="region-20" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -380px;"></span><span class="region-label">Georgia</span></option>
				<option id="region-21" class=" fav" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -400px;"></span><span class="region-label">Germany</span></option>
				<option id="region-22" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -420px;"></span><span class="region-label">Greece</span></option>
				<option id="region-23" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -440px;"></span><span class="region-label">Hungary</span></option>
				<option id="region-24" class=" fav" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -460px;"></span><span class="region-label">India</span></option>
				<option id="region-25" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -480px;"></span><span class="region-label">Iran</span></option>
				<option id="region-26" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -500px;"></span><span class="region-label">Israel</span></option>
				<option id="region-27" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -520px;"></span><span class="region-label">Italy</span></option>
				<option id="region-28" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -540px;"></span><span class="region-label">Japan</span></option>
				<option id="region-29" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -560px;"></span><span class="region-label">Korea</span></option>
				<option id="region-30" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -580px;"></span><span class="region-label">Mexico</span></option>
				<option id="region-31" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -600px;"></span><span class="region-label">Morocco</span></option>
				<option id="region-32" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -620px;"></span><span class="region-label">Netherlands</span></option>
				<option id="region-33" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -640px;"></span><span class="region-label">New Zealand</span></option>
				<option id="region-34" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -660px;"></span><span class="region-label">Nigeria</span></option>
				<option id="region-35" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -680px;"></span><span class="region-label">Norway</span></option>
				<option id="region-36" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -700px;"></span><span class="region-label">Pakistan</span></option>
				<option id="region-37" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -720px;"></span><span class="region-label">Poland</span></option>
				<option id="region-38" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -740px;"></span><span class="region-label">Portugal</span></option>
				<option id="region-39" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -760px;"></span><span class="region-label">Romania</span></option>
				<option id="region-40" class=" fav" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -780px;"></span><span class="region-label">Russia</span></option>
				<option id="region-41" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -800px;"></span><span class="region-label">Slovakia</span></option>
				<option id="region-42" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -820px;"></span><span class="region-label">Slovenia</span></option>
				<option id="region-43" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -840px;"></span><span class="region-label">Spain</span></option>
				<option id="region-44" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -860px;"></span><span class="region-label">Sweden</span></option>
				<option id="region-45" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -880px;"></span><span class="region-label">Switzerland</span></option>
				<option id="region-46" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -900px;"></span><span class="region-label">Turkey</span></option>
				<option id="region-47" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -920px;"></span><span class="region-label">Ukraine</span></option>
				<option id="region-49" class="" role="option" tabindex="-1"><span class="flag" style="background-position: 0 -960px;"></span><span class="region-label">Vietnam</span></option>
			</select>
		</div>
	</div> 	<br>
	<div class="row">
		<div class="col-md-3"><label>Email Domains: </label></div>
		<div class="col-md-4">
			<textarea class="form-control" cols='60' rows='5' id="emailDomains">yahoo.com|10&#13;gmail.com|20&#13;me.com</textarea>
		</div>
	</div>	<br>
	<div class="row">
		<div class="col-md-3"><label>Username Prefix/Suffix: </label></div>
		<div class="col-md-4">
			<textarea class="form-control" cols='60' rows='5' id="presuffix">plumber|10&#13;electrician|20&#13;carpenter|20&#13;solar installer</textarea>
		</div>
	</div>		
	<br>
	<br>
	<button class="btn btn-primary" id="loadUsers">Load Users</button>
	</div>
</div>
