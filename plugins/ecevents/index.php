<?php
 /*
    Plugin Name: EC Events
    Description: EC plugin Creates Events
    Author: Earl Cariño (earlrodson@gmail.com)
    Version: 1.1
*/

CLASS ecEvents {
	function __construct() {
        add_action( 'init',                 array($this, 'create_posttype') );
	}



    /* Register post type */
    public function create_posttype() {
         
            register_post_type( 'events',
            // CPT Options
                array(
                    'labels' => array(
                        'name' => __( 'Events' ),
                        'singular_name' => __( 'Events' )
                    ),
                    'public' => true,
                    'has_archive' => true,
                    'rewrite' => array('slug' => 'events'),
                )
            );

        }



    /* PROCESS */
    public function main_process() {

        global $wpdb;
  		


        wp_die();

    }    
    /* END PROCESS */
}
NEW ecEvents;